@echo off
setlocal
set file="C:\Windows\system.ini"
%1 %0 :: %file%
set "str=%~z2"
echo Le fichier %file% fait %str:~-12,-9% %str:~-9,-6% %str:~-6,-3% %str:~-3% octets
if "%str:~-12,-9%" NEQ "" (
	echo soit %str:~-12,-9% GO
)
set /p choix=Voulez-vous le supprimer ? [y/n]
if %choix% EQU y (
	echo Mauvaise idee
)
pause