#!/bin/bash

echo "Veuillez branchez votre périphérique de stockage"
echo "Laissez le branche jusqu'a la fin du processus"
echo
read -p "Appuyer sur entrée lorsque votre périphérique est inséré"
clear
cat distro.csv | sed -e 's/,,/, ,/g' | column -s, -t
echo
echo
read -p "Quelle distribution recherchez-vous ? " build

location=0
if [ $build = "ubuntu" ]
then
	name="ubuntu-20.04.5-desktop-amd64.iso"
	url="http://releases.ubuntu.com/focal/"$name
	location=1
elif [ $build = "server" ]
then
	name="ubuntu-20.04.5-live-server-amd64.iso"
	url="https://releases.ubuntu.com/focal/"$name
	location=1
elif [ $build = "debian" ]
then
	name="debian-11.5.0-amd64-netinst.iso"
	url="https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/"$name
	location=1
elif [ $build = "arch" ]
then
	name="archlinux-x86_64.iso"
	url="https://archlinux.mailtunnel.eu/iso/latest/"$name
	location=1
elif [ $build = "mint" ]
then
	name="linuxmint-19.3-cinnamon-32bit.iso"
	url="http://ftp.crifo.org/mint-cd/stable/19.3/"$name
	location=1
fi

if [ $location == 0 ]
then
	echo "Nous n'avons pas trouvé la distribution demandée ! Pardon !"
	sleep 2
else
	echo "Nous avons trouvé votre distribution !"
	echo "Downloading $url"
	wget $url

	if [ -f $name ]
	then
		sudo fdisk -l

		read -p "Rentrez le peripherique recherche : " cle

		sudo dd if=$name of=/dev/$cle bs=4M status=progress && sync

		rm $name
		sleep 0.5
	else
		echo "Il y a eu un problème dans le téléchargement de l'image disque !"
		sleep 2
	fi
fi
clear
clear